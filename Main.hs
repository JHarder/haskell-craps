module Main where

import           Control.Applicative ((<$>))
import           Control.Monad       (liftM, replicateM)
import           System.Random       (randomRIO)

data Die = One | Two | Three | Four | Five | Six
           deriving (Show, Eq, Ord, Enum)

type Dice = [Die]

rollDie :: IO Die
rollDie = liftM toEnum $ randomRIO (0,5)

rollDice :: Int -> IO [Die]
rollDice = flip replicateM rollDie

-- currently dice go from 0 to 5
rollScore :: Dice -> Int
rollScore dice = length dice + sum (fromEnum <$> dice)
 

play :: Maybe Dice -> IO Bool
play = maybe setPoint playAfterPoint

setPoint :: IO Bool
setPoint = do
         roll <- rollDice 2 --roll once, then set the point
         let score = rollScore roll
         putStrLn $ "You rolled " ++ show roll -- score
         if score `elem` [2,3,12]
             then return False -- you lose
             else if score `elem` [7,11]
                      then return True -- you Win!
                      else do
                        putStrLn $ "The point is set to " ++ show score
                        play $ Just roll -- score is elem of [4,5,6,8,9,10] and we play another round

playAfterPoint :: Dice -> IO Bool
playAfterPoint point = do
        roll <- rollDice 2
        let score = rollScore roll
            pointScore = rollScore point
        if score == pointScore
            then do
              putStrLn $ "You rolled a " ++ show score ++ ", matching the point."
              return True -- you Win!
            else
                if score == 7
                     then putStrLn ("you rolled a " ++ show score ++ " after the point was set.") >> return False -- you lose!
                     else do
                       putStrLn ("you rolled a " ++ show score ++ ", rolling again.")
                       play $ Just point

main :: IO ()
main = do
    result <- play Nothing
    putStrLn (if result then "You win!!" else "You lose!!")
